
# For better performance, replace ``-O'' with whatever
# the best optimization flag is for your computer.
# For Sun's compilers under Solaris, ``-fast'' works well.
# For gcc, ``-O2'' works well.
CC=cc
CFLAGS=-O
LDLIBS=-lm
 
sim4:
	$(CC) -o sim4 -I. $(CFLAGS) *.c $(LDLIBS)
clean:
	rm -f sim4 *.o
