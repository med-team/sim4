#ifndef LIBC_H
#define LIBC_H
/*$Id: libc.h,v 1.2 2000/10/19 22:17:03 schwartz Exp $*/
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#endif
